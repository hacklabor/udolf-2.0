// Visual Micro is in vMicro>General>Tutorial Mode
// 
/*
    Name:       Udolf_2.0.ino
    Created:	21.10.2018 11:51:50
    Author:     Thk
*/

// Define User Types below here or use a .h file
//


// Define Function Prototypes that use User Types below here or use a .h file



// Define Functions below here or use other .ino or cpp files
//

// The setup() function runs once each time the micro-controller starts
#include <gamma.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>
#ifndef PSTR
#define PSTR // Make Arduino Due happy
#endif

#define PIN 14 //D5 Beim ESP8266 WemosMini

// MATRIX DECLARATION:
// Parameter 1 = width of NeoPixel matrix
// Parameter 2 = height of matrix
// Parameter 3 = pin number (most are valid)
// Parameter 4 = matrix layout flags, add together as needed:
//   NEO_MATRIX_TOP, NEO_MATRIX_BOTTOM, NEO_MATRIX_LEFT, NEO_MATRIX_RIGHT:
//     Position of the FIRST LED in the matrix; pick two, e.g.
//     NEO_MATRIX_TOP + NEO_MATRIX_LEFT for the top-left corner.
//   NEO_MATRIX_ROWS, NEO_MATRIX_COLUMNS: LEDs are arranged in horizontal
//     rows or in vertical columns, respectively; pick one or the other.
//   NEO_MATRIX_PROGRESSIVE, NEO_MATRIX_ZIGZAG: all rows/columns proceed
//     in the same order, or alternate lines reverse direction; pick one.
//   See example below for these values in action.
// Parameter 5 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)


// Example for NeoPixel Shield.  In this application we'd like to use it
// as a 5x8 tall matrix, with the USB port positioned at the top of the
// Arduino.  When held that way, the first pixel is at the top right, and
// lines are arranged in columns, progressive order.  The shield uses
// 800 KHz (v2) pixels that expect GRB color data.
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, PIN,
	NEO_MATRIX_TOP + NEO_MATRIX_RIGHT +
	NEO_MATRIX_COLUMNS + NEO_MATRIX_PROGRESSIVE,
	NEO_GRB + NEO_KHZ800);

struct Eye
{
	int x;
	int y;
};
struct Eye Eyepos;

const uint16_t colors[] = {
  matrix.Color(255, 255, 255) };

void setup() {
	matrix.begin();
	matrix.setTextWrap(false);
	matrix.setBrightness(10);
	
	matrix.setTextColor(colors[0]);
	Eyepos.x = 3;
		Eyepos.y = 3;
}





void loop() {
	matrix.fillScreen(colors[0]);
	//matrix.setPixelColor(0,0,0,0);
	//matrix.setPixelColor(1, 0, 0, 0);
	//matrix.setPixelColor(6, 0, 0, 0);
	//matrix.setPixelColor(7, 0, 0, 0);
	//matrix.setPixelColor(8, 0, 0, 0);
	//matrix.setPixelColor(15, 0, 0, 0);
	//matrix.setPixelColor(48, 0, 0, 0);
	//matrix.setPixelColor(55, 0, 0, 0);
	//matrix.setPixelColor(56, 0, 0, 0);
	//matrix.setPixelColor(57, 0, 0, 0);
	//matrix.setPixelColor(62, 0, 0, 0);
	//matrix.setPixelColor(63, 0, 0, 0);

	int richtung = random(1,100);
	
	if (richtung < 9) {
		struct Eye EyeposNew = chk_eye_move(richtung, Eyepos.x, Eyepos.y);
		if (EyeposNew.x != -1 && EyeposNew.y != -1) {
			Eyepos.x = EyeposNew.x;
			Eyepos.y = EyeposNew.y;
		}

	}

	matrix.drawRect(Eyepos.x, Eyepos.y, 2, 2, 0);
	matrix.show();
	
	if (random(0, 30) == 0) {
		zwinker_eye(20);
		matrix.drawRect(Eyepos.x, Eyepos.y, 2, 2, 0);
		matrix.show();
	}
	

	
	matrix.show();
	delay(100);
	
}



void zwinker_eye(int delaytime) {
	int color = 0;
	
	drawLinie(0, 0, 7, color, delaytime);
	drawLinie(7, 0, 7, color, delaytime);

	drawLinie(1, 0, 7, color, delaytime);
	drawLinie(6, 0, 7, color, delaytime);

	drawLinie(2, 0, 7, color, delaytime);
	drawLinie(5, 0, 7, color, delaytime);

	drawLinie(3, 0, 7, color, delaytime);
	drawLinie(4, 0, 7, color, delaytime);

	color = colors[0];

	drawLinie(3, 0, 7, color, delaytime);
	drawLinie(4, 0, 7, color, delaytime);

	drawLinie(2, 0, 7, color, delaytime);
	drawLinie(5, 0, 7, color, delaytime);

	drawLinie(1, 0, 7, color, delaytime);
	drawLinie(6, 0, 7, color, delaytime);

	drawLinie(0, 0, 7, color, delaytime);
	drawLinie(7, 0, 7, color, delaytime);
	
	
}

void drawLinie(int row, int collumStart, int collumEnd, int color,int delaytime) {
	matrix.drawLine(row, collumStart, row, collumEnd, color);
	matrix.show();
	delay(delaytime);
}

struct Eye chk_eye_move(int direction, int x, int y) {
	struct Eye eyetemp;
	eyetemp.x = -1;
	eyetemp.y = -1;
	
	if (direction == 1) {  // Up + Left
		x -= 1;
		y -= 1;
	}
			if (direction == 2) { // Up + Left
				y -= 1;
			}
			if (direction == 3) {
				// Up + Left
				x += 1;
				y -= 1;
			}
			if (direction == 4) { // Up + Left
				x += 1;
			}
			if (direction == 5) { // Up + Left
				x += 1;
				y += 1;
			}
			if (direction == 6) { // Up + Left
				y += 1;
			}
			if (direction == 7){ // Up + Left
				x -= 1;
				y += 1;
		}
				if (direction == 8){ // Up + Left
			x -= 1;
		}
										if ((x < 2) || x > 5 || y < 1 || y > 5) {
											
											return eyetemp;
										}
										if ((x == 0 && y == 0) || (x == 1 && y == 0) || (x == 0 && y == 1) || (x == 1 && y == 1)) {
											//return eyetemp;
										}
	if ((x == 0 && y == 6) || (x == 1 && y == 6) || (x == 0 && y == 5) || (x == 1 && y == 5)){
		//return eyetemp;
	}
		if ((x == 5 && y == 0) || (x == 6 && y == 1) || (x == 6 && y == 0 || (x == 5 && y == 1))){
		//	return eyetemp;
		}

			if ((x == 5 && y == 7) || (x == 6 && y == 6) || (x == 6 && y == 7) || (x == 5 && y == 5)){
				//return eyetemp;
			}

			eyetemp.x = x;
				eyetemp.y = y;

				return eyetemp;
}

	

	